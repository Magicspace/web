from django.shortcuts import render
import datetime
#from passlib.hash import sha256_crypt 
from django.http import HttpResponse, HttpResponseBadRequest, HttpResponseForbidden
from signup.models import Users

# Create your views here.

def is_username_valid(username):
    if username: return True
    return False



def is_password_valid(password):
    if password: return True
    return False


def index(request):
    
    if request.method != "POST":
        return HttpResponseBadRequest("Error: Send your data via POST.")

    post = request.POST.dict()

    # whether data is valid.

    if (set(post.keys()) != {"username", "password"}
        or not is_username_valid(post["username"].lower())
        or not is_password_valid(post["password"])
):
        return HttpResponseBadRequest("Error: Your POST data is corrupted.")

    # checking user from database.

    try:
        user = Users.objects.get(username=post["username"].lower())
    except Users.DoesNotExist:
        return HttpResponseForbidden("No such user exists.")

    # Verify password_hash and intered password.

    if not post["password"] != user.password_hash:
        return HttpResponseForbidden("Your password is incorrect.")
    
    # Setting username for session.

    request.session['username'] = user.username
    return HttpResponse(
        "Welcome %s! Your entered time is: %s"
        % ( user.name,
            str(datetime.datetime.now())

))
