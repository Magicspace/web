from django.shortcuts import render

# Create your views here.

def signout(request):
    
    # Checking whether the user is signed in.

    if request.session.has_key("username"):
        user = Users.objects.get(username=request.session['username'])
        del request.session['username']
        return HttpResponse("Dear %s, You are logged out now!" % user.name)
    else:
        return HttpResponseBadRequest("You are not logged in.")
