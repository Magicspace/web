from django.shortcuts import render
import datetime
from django.http import HttpResponse, HttpResponseForbidden, HttpResponseBadRequest
from signup.models import Users

# Create your views here.

def dummy(request):
    
    # Checking whether the user is signed in.

    if request.session.has_key("username"):
        username = request.session["username"]
        user = Users.objects.get(username=username)
        return HttpResponse("Dear %s here is your dummypage!\nThe time is:%s" % (user.name, str(datetime.datetime.now()))
)
    else:
        return HttpResponseForbidden("You are not logged in!")

