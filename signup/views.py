from django.shortcuts import render
#from passlib.hash import sha256_crypt #@UnresolvedImport
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseBadRequest
from signup.models import Users
# Create your views here.

"""Check whether username is valid or not."""
def is_username_valid(username):
    if (3 <= len(username) <= 100
        and username.replace('_', '').isalnum()
        and (username[0].isalpha() or username[0] == '_')
):

        return True
    return False


"""Check whether password is valid or not."""
def is_password_valid(password):
    if len(password) >= 8:
        return True
    return False


"""Check whether name is valid or not."""
def is_name_valid(name):
    if 0 <= len(name) <= 100:
        return True
    return False

"""Signing up on the site."""
def index(request):
    if request.method != "POST":
        return HttpResponseBadRequest("Send your data via POST!")

    post = request.POST.dict()
    
    #whether data is valid.
    
    if (set(post.keys()) != {"name", "username", "password"} 
        or not is_name_valid(post["name"])
        or not is_username_valid(post["username"].lower())
        or not is_password_valid(post["password"])
):
        return HttpResponseBadRequest("Your POST data is corrupted!")

    #if username exists.

    if Users.objects.filter(username=post["username"].lower()):

        return HttpResponseBadRequest("This username already exists.")

    # Hashing password.

    #password_hash = sha256_crypt.encrypt(post['password'],
                                          #rounds=100000,
                                          #salt_size=16)

    # Creating user.

    Users.objects.create(name=post['name'],

                         username=post['username'].lower(),

                         password_hash=post['password'],

)

    return HttpResponse("Registration has been completed.")
